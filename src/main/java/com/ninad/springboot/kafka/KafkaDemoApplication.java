package com.ninad.springboot.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class KafkaDemoApplication implements ApplicationRunner {

	private static final String TOPIC = "kafka-spring-boot";

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public void sendMessage(String msg) {
		System.out.println("===> ");
		kafkaTemplate.send(TOPIC, msg);
	}

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(
				KafkaDemoApplication.class, args);
	}

	@KafkaListener(topics = TOPIC, group = "group-id")
	public void listen(String message) {
		System.out.println("Received Messasge in group - group-id: " + message);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		sendMessage("Welcome to kafka world !!!");
	}
}